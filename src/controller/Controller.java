package controller;

import api.ITaxiTripsManager;
import model.data_structures.BinaryHeap;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.RangoFechaHora;
import model.vo.Taxi;

/**
 * Clase que controla de manera est�tica los m�todos que contienen los requerimientos del proyecto.
 */
public class Controller 
{
	/**
	 * Modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	/**
	 * Carga el sistema Json para manejar el programa.
	 * @param direccionJson Nombre del json que se quiere cargar.
	 * @return True si se pudo cargar el sistema Json. False de lo contrario.
	 */
	public static boolean cargarSistema(String direccionJson, RangoFechaHora r)
	{
		return manager.cargarSistema(direccionJson, r);
	}
	public static BinaryHeap<Taxi> heapSortOfTaxis()
	{
		return manager.heapSortOfTaxis();
	}
	public static BinaryHeap<Compania>getCompaniesInOrderByNumOfServices()
	{
		return manager.getCompaniesInOrderByNumOfServices();
	}
}
