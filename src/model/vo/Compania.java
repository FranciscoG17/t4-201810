package model.vo;

import model.data_structures.BinaryHeap;

public class Compania implements Comparable<Compania> 
{
	//--------------------------------
	//ATRIBUTOS
	//--------------------------------
	
	/**
	 * Nombre de las compa�ia.
	 */
	private String nombre;
	/**
	 * Lista de taxis compania
	 */
	private BinaryHeap<Taxi> taxisCompania;
	
	/**
	 * Numero de taxis compania
	 */
	private int numTaxisCompania;
	
	/**
	 * Numero de servicios en rango
	 */
	private int numServiciosEnRango;
	
	private int numTaxisEnRango;
	
	
	//--------------------------------
	//CONSTRUCTOR
	//--------------------------------
	
	/**
	 * Constructor Compania	
	 * @param company_name Nombre de la compania
	 */
	public Compania(String company_name) 
	{ 
		nombre = company_name;
		taxisCompania = new BinaryHeap<Taxi>();
		numTaxisCompania = 0;
		numTaxisEnRango = 0;
		numServiciosEnRango = 0;
	}
	
	//--------------------------------
	//METODOS
	//--------------------------------
	
	/**
	 * Nombre de la compania
	 * @return String nombre
	 */
	public String getNombre() 
	{
		return nombre;
	}
	/**
	 * Numero de taxis de la compania
	 * @return int numTaxisCompania
	 */
	public int getNumTaxisCompania()
	{
		return numTaxisCompania;
	}
	/**
	 * Numero de servicios en rango de la compania
	 * @return int numServiciosEnRango
	 */
	public int getNumServiciosEnRango()
	{
		return numServiciosEnRango;
	}
	/**
	 * Lista de taxis compania
	 * @return ArrayListT taxisCompania
	 */
	public BinaryHeap<Taxi> getTaxisCompania() 
	{
		return taxisCompania;
	}
	/**
	 * A�ade a la lista de taxis compania
	 */
	public Taxi addTaxisCompania(Taxi element)
	{
		Taxi t = taxisCompania.add(element);
		numTaxisCompania++;
		return t;
	}
	/**
	 * Busca binariamente un taxi
	 * @param pTaxiId id del taxi a buscar
	 * @return Taxi taxi a buscar
	 */
	public Taxi binarySearchTaxi(String pTaxiId)
	{
		ordenarTaxisCompania(true);
		Taxi tx = null;
		boolean existe = false;

		int inicio = 0;
		int fin = taxisCompania.size() - 1;

		while( inicio <= fin && ! existe)
		{
			int medio = (inicio+fin)/2;
			int res = taxisCompania.get(medio).getTaxiId().compareTo(pTaxiId);
			if(res == 0)
			{
				existe = true;
				tx = taxisCompania.get(medio);
			}
			else if(res > 0)

			{
				fin = medio-1;
			}
			else if(res < 0)
			{
				inicio = medio+1;
			}
		}
		return tx;
	}
	/**
	 * Ordenar taxis de una compania
	 */
	public void ordenarTaxisCompania(boolean ascendente)
	{
		taxisCompania.sort(ascendente);
	}
	
	/**
	 * Verifica la existencia de un un taxi
	 * @param pId id del taxi a buscar
	 * @return Taxi taxi a buscar
	 */
	public Taxi existsTaxi(String pId)
	{
		boolean existe = false;
		Taxi miTaxi = null;
		if(!taxisCompania.isEmpty())
		{
			for(int j=0; j< taxisCompania.size() && !existe; j++)
			{
				if(taxisCompania.get(j).getTaxiId().contains(pId))
				{
					miTaxi = taxisCompania.get(j);
					existe = true;
				}
			}
		}
		return miTaxi;
	}
	
	/**
	 * Asignar numero de servicios en rango
	 * @param r RangoFechaHora
	 */
	public void asignarNumServiciosDeLaCompaniaEnRango()
	{
		numServiciosEnRango ++;
	}
	public void asignarNumDeTaxisEnRango()
	{
		numTaxisEnRango++;
	}
	public int getNumTaxisInRange()
	{
		return numTaxisEnRango;
	}
	
	/**
	 * @param o Compania compania con la que se comparara
	 * @return int resultado de comparar una compania
	 */
	@Override
	public int compareTo(Compania o) 
	{
		int compare = numServiciosEnRango- o.getNumServiciosEnRango();
		if(compare < 0)
		{
			return -1;
		}
		else if(compare > 0)
		{
			return 1;
		}
		else
		{
			return 0;			
		}
	}
	/**
	 * @return nombre de la compania
	 */
	public String toString()
	{
		return nombre;
	}

}