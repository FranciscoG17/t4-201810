package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private String taxi_id;
	private String company;
	private int numServicesInRange;
	private double plataGanada;
	private double distanciaTotal;


	public Taxi(String pTaxi_id, String company_name)
	{
		taxi_id = pTaxi_id;
		company = company_name;
		numServicesInRange = 0;
		plataGanada = 0;
		distanciaTotal = 0.0;
	}

	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		return  taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		return company;
	}
	public void asignarNumeroServiciosEnRango()
	{
		numServicesInRange ++;
	}
	public void asignarDineroEnRango(double trip_total)
	{
		plataGanada += trip_total;
	}
	public void asignarDistanciaEnRango(double pDis)
	{
		distanciaTotal += pDis;
	}
	public double darPlataGanadaEnRango()
	{
		return plataGanada;
	}
	public int darNumeroServiciosEnRango()
	{
		return numServicesInRange;
	}
	public double darDistanciaEnRango()
	{
		return distanciaTotal;
	}
//
//	public int getTiempoServicios()
//	{
//		return tiempoServicios;
//	}
//
//	public BinaryHeap<Servicio> getTaxiServices()
//	{
//		return serviciosDelTaxi;
//	}
//
//	public InfoTaxiRango getInfoTaxiRango()
//	{
//		return infoTaxiRango;
//	}
//
//	public int getNumServicesInRange()
//	{
//		return numServicesInRange;
//	}
//	public double  getRentabilidad()
//	{
//		rentabilidad = plataGanada/ serviciosDelTaxi.size();
//		return rentabilidad;
//	}
//
//	public double getPlataGanada()
//	{
//		return plataGanada;
//	}
//
//	public double getDistanciaRecorrida()
//	{
//		return distanciaTotal;
//	}
//
//	public void asignarPlataGanada(RangoFechaHora r)
//	{
//		try
//		{
//			for(int j=0; j < serviciosDelTaxi.size(); j++)
//			{
//				if(serviciosDelTaxi.get(j).estaEnElRango(r))
//					plataGanada += serviciosDelTaxi.get(j).getTripTotal();
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//
//	public void asignarNumeroServiciosEnRango(RangoFechaHora r)
//	{
//		try
//		{
//			for(int i=0; i < serviciosDelTaxi.size(); i++)
//			{
//				if(serviciosDelTaxi.get(i).estaEnElRango(r))
//					numServicesInRange++;
//			}
//		}
//		catch(Exception e )
//		{
//			e.printStackTrace();
//		}
//	}
//
//	public BinaryHeap<Servicio>asignarServiciosEnRango(RangoFechaHora r)
//	{
//		BinaryHeap<Servicio> arr = new BinaryHeap<Servicio>();
//		try
//		{
//			for(int i=0; i < serviciosDelTaxi.size(); i++)
//			{
//				if(serviciosDelTaxi.get(i).estaEnElRango(r))
//				{
//					arr.add(serviciosDelTaxi.get(i));
//					numServicesInRange++;
//				}
//			}
//			return arr;
//		}
//		catch(Exception e )
//		{
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	public void asignarDistanciaServiciosEnRango(RangoFechaHora r)
//	{
//		try
//		{
//			for(int i=0; i < serviciosDelTaxi.size(); i++)
//			{
//				if(serviciosDelTaxi.get(i).estaEnElRango(r))
//					distanciaTotal += serviciosDelTaxi.get(i).getTripMiles();
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//	public void asignarTiempoServiciosEnRango(RangoFechaHora r)
//	{
//		try
//		{
//			for(int i=0; i < serviciosDelTaxi.size(); i++)
//			{
//				if(serviciosDelTaxi.get(i).estaEnElRango(r))
//					tiempoServicios += serviciosDelTaxi.get(i).getTripSeconds();
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//	
	@Override
	public int compareTo(Taxi o) 
	{
		int compare = taxi_id.compareTo(o.taxi_id);
		if(compare < 0)
		{
			return -1;
		}
		else if(compare > 0)
		{
			return 1;
		}
		else
		{
			return 0;			
		}
	}
//
//	public String toString()
//	{
//		return taxi_id;
//	}
}