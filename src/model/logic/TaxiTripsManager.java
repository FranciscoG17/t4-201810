package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.BinaryHeap;
import model.vo.Compania;
import model.vo.RangoFechaHora;
import model.vo.Taxi;

public class TaxiTripsManager implements ITaxiTripsManager 
{


	/**
	 * Constantes que tienen las rutas de los JSON
	 */
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";

	/**
	 * Constante COMPANY
	 */
	public static final String COMPANY = "company";
	
	/**
	 * Constante TRIP_ID
	 */
	public static final String TRIP_ID = "trip_id";
	
	/**
	 * Constante TAXI_ID
	 */
	public static final String TAXI_ID ="taxi_id";
	
	/**
	 * Constante TRIP_SECONDS
	 */
	public static final String TRIP_SECONDS ="trip_seconds";
	
	/**
	 * Constante TRIP_MILES
	 */
	public static final String TRIP_MILES ="trip_miles";
	
	/**
	 * Constante TRIP_TOTAL
	 */
	public static final String TRIP_TOTAL ="trip_total";
	
	/**
	 * Constante DROPOFF_AREA
	 */
	public static final String DOFF_AREA ="dropoff_community_area";
	
	/**
	 * Constante PICK_UP_AREA
	 */
	public static final String PUP_AREA = "pickup_community_area";
	
	/**
	 * Constante TRIP_START_TIMESTAMP
	 */
	public static final String START_TIME = "trip_start_timestamp";
	
	/**
	 * Constante TRIP_END_TIMESTAMP
	 */
	public static final String END_TIME = "trip_end_timestamp";

	//--------------------------------
	//ATRIBUTOS
	//--------------------------------

	/**
	 * Lista de servicios.
	 */
	private BinaryHeap<Compania> companias;

	/**
	 * Lista de taxis.
	 */
	private BinaryHeap<Taxi> taxis;

	//--------------------------------
	//CONSTRUCTOR
	//--------------------------------

	/**
	 * Constructor TaxiTripsManager
	 */
	public TaxiTripsManager()
	{
		companias = new BinaryHeap<Compania>();
		taxis = new BinaryHeap<Taxi>();
	}

	//--------------------------------
	//METODOS
	//--------------------------------

	/**
	 * Carga el sistemam del TTM
	 * @param direccionJson Direcci�n JSON
	 * @return True si carg�.
	 */
	public boolean cargarSistema(String direccionJson,RangoFechaHora r) 
	{
		JsonParser parser = new JsonParser();
		try 
		{
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(direccionJson));
			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				String company_name = obj.get(COMPANY) != null ? obj.get(COMPANY).getAsString() : "Independent Owner";

				String taxi_id = obj.get(TAXI_ID) != null ? obj.get(TAXI_ID).getAsString() : "NaN";

				double trip_miles = obj.get(TRIP_MILES) != null ? obj.get(TRIP_MILES).getAsDouble() : -1.0;

				double trip_total = obj.get(TRIP_TOTAL) != null ? obj.get(TRIP_TOTAL).getAsDouble() : 0.0;

				String fi = obj.get(START_TIME) != null ? obj.get(START_TIME).getAsString() : "2025-09-24T00:00:00.000";

				String ff = obj.get(END_TIME) != null ? obj.get(END_TIME).getAsString() : "2025-09-24T00:00:00.000";
				
				if(r.getFechaHoraInicial().compareTo(fi) <= 0 && r.getFechaHoraFinal().compareTo(ff) >= 0)
				{
					Compania companiaActual =  existsCompany(company_name);
					if(companiaActual == null)
					{
						companiaActual = companias.add(new Compania(company_name));
					}
					Taxi taxi = existsTaxi(taxi_id);
					if(taxi == null)
					{
						taxi = companiaActual.addTaxisCompania(new Taxi(taxi_id, company_name));
						taxis.add(new Taxi(taxi_id, company_name));
						companiaActual.asignarNumDeTaxisEnRango();
					}
					taxi.asignarNumeroServiciosEnRango();
					taxi.asignarDineroEnRango(trip_total);
					taxi.asignarDistanciaEnRango(trip_miles);
					companiaActual.asignarNumServiciosDeLaCompaniaEnRango();
				}
			}
		}
		catch (JsonIOException e1 ) 
		{
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) 
		{
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) 
		{
			e3.printStackTrace();
		} 

		return false;
	}

	/**
	 * Verifica la existencia de un un taxi
	 * @param pId id del taxi a buscar
	 * @return Taxi taxi a buscar
	 */
	public Taxi existsTaxi(String pId)
	{
		boolean existe = false;
		Taxi miTaxi = null;
		for(int i=1; i< taxis.size()+1 && !existe; i++)
		{
			if(taxis.get(i) != null && taxis.get(i).getTaxiId().equals(pId))
			{
				miTaxi = taxis.get(i);
				existe = true;
			}
		}
		return miTaxi;
	}
	/**
	 * Verifica la existencia de un un taxi
	 * @param pId id del taxi a buscar
	 * @return Taxi taxi a buscar
	 */
	public Compania existsCompany(String pNombre)
	{
		boolean existe = false;
		Compania compania = null;
		for(int i=1; i< companias.size()+1 && !existe; i++)
		{
			if(companias.get(i).getNombre().equals(pNombre))
			{
				compania = companias.get(i);
				existe = true;
			}
		}
		return compania;
	}

	//--------------------------------
	//METODOS REQUERIMIENTOS
	//--------------------------------

	public BinaryHeap<Taxi> heapSortOfTaxis()
	{
		taxis.sort(true);
		return taxis;
	}

	public BinaryHeap<Compania>getCompaniesInOrderByNumOfServices()
	{
		companias.sort(false);
		return companias;
	}

}