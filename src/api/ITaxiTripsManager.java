package api;

import model.data_structures.BinaryHeap;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.vo.Compania;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.ZonaServicios;

/**
 * API para la clase de logica principal  
 */
public interface ITaxiTripsManager 
{
	/**
	 * Dada la direcci�n del json que se desea cargar, se generan VO's, estructuras y datos necesarias
	 * @param direccionJson, ubicaci�n del json a cargar
	 * @return true si se lo logr� cargar, false de lo contrario
	 */
	public boolean cargarSistema(String direccionJson,RangoFechaHora r);
	
	public BinaryHeap<Taxi> heapSortOfTaxis();
	
	public BinaryHeap<Compania>getCompaniesInOrderByNumOfServices();


}
