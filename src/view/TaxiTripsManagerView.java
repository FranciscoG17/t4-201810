package view;

import java.io.File;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.BinaryHeap;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.RangoFechaHora;
import model.vo.Taxi;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}
				System.out.println("Ingrese la fecha inicial - Ej:(2017-02-01)");
				String fechaInicial = sc.next();
				System.out.println("Ingrese la hora inicial - Ej:(17:00:00.000)");
				String horaInicial = sc.next();
				System.out.println("Ingrese la fecha final - Ej:(2017-02-01)");
				String fechaFinal = sc.next();
				System.out.println("Ingrese la hora final - Ej:(19:00:00.000)");
				String horaFinal = sc.next();

				RangoFechaHora r = new RangoFechaHora(fechaInicial, fechaFinal, horaInicial, horaFinal);

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();


				if(linkJson.equals(TaxiTripsManager.DIRECCION_LARGE_JSON))
				{
					System.out.println(linkJson);
					File arch = new File(linkJson);
					if ((arch.exists())) 
					{
						File[] files = arch.listFiles();
						for (File file : files) 
						{
							if(file.isFile())
							{
								System.out.println("./data/taxi-trips-wrvz-psew-subset-large/"+file.getName());
								Controller.cargarSistema("./data/taxi-trips-wrvz-psew-subset-large/"+file.getName(), r);
							}
						}
					}
				}
				else
				{
					Controller.cargarSistema(linkJson, r);
				}
				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1A	
			case 2:

				BinaryHeap<Taxi> bh= Controller.heapSortOfTaxis();
				for (int i = 1; i < bh.size()+1; i++) 
				{
					System.out.println("El taxi con id: " + bh.get(i).getTaxiId());
					System.out.println("Numero de servicios en rango: " + bh.get(i).darNumeroServiciosEnRango());
					System.out.println("Distancia recorrida: " + bh.get(i).darDistanciaEnRango() + " millas");
					System.out.println("Dinero ganado: $" + bh.get(i).darPlataGanadaEnRango());
					System.out.println("------------------------------------------------------------------------");
				}

				break;

			case 3: //2A
				BinaryHeap<Compania> bhCompanias= Controller.getCompaniesInOrderByNumOfServices();
				for (int i = 1; i <bhCompanias.size()+1; i++) 
				{
					System.out.println("-----------------------");
					System.out.println("La compania: "+ bhCompanias.get(i).getNombre() + " cuenta con " + bhCompanias.get(i).getNumServiciosEnRango() + " servicios." );
					System.out.println("Además, cuenta con "+  bhCompanias.get(i).getNumTaxisInRange() +" taxis en el rango dado");
				}


				break;

			case 4: //Salir
				fin=true;
				sc.close();
				break;
			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cargar las compañias y los taxis con su información en un rango de tiempo reportados en un subconjunto de datos");
		System.out.println("2. Ordenar taxis por su id mediante heapSort ");
		System.out.println("3. Verificar el ordenamiento de los servicios del taxi por tiempo de inicio");
		System.out.println("4. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");


	}

}
