package test.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.BinaryHeap;

public class BinaryHeapTest 
{
	private BinaryHeap<String> array;

	@Before
	public void setupEscenario1()
	{
		array = new BinaryHeap<String>();
		for(int i= 1 ; i < 81; i++)
		{
			int cont = i;
			if(i < 10)
				array.add("H0"+cont);
			else
				array.add("H"+cont);
		}
	}

	@Test
	public void addTest()
	{
		setupEscenario1();

		assertTrue("Estan mal puestos como heap", array.get(1).compareTo(array.get(2)) >= 0 && array.get(1).compareTo(array.get(3)) >= 0);
		assertTrue("Estan mal puestos como heap", array.get(2).compareTo(array.get(4)) >= 0 && array.get(2).compareTo(array.get(5)) >= 0);
		assertTrue("Estan mal puestos como heap", array.get(3).compareTo(array.get(6)) >= 0 && array.get(3).compareTo(array.get(7)) >= 0);
		assertTrue("Estan mal puestos como heap", array.get(4).compareTo(array.get(8)) >= 0 && array.get(4).compareTo(array.get(9)) >= 0);
	}

	@Test
	public void getTest()
	{
		setupEscenario1();

		assertEquals("No es igual al elemento esperado", "H"+ array.size(), array.get(1));	
		assertEquals("No es igual al elemento esperado", "H03", array.get(array.size()));	

	}

	@Test
	public void removeTest()
	{
		setupEscenario1();
		int n = array.size();

		String removido = array.remove();

		assertEquals("Deberia haber borrado el elemento mayor", "H"+ (array.size()+1),removido);

		for (int i = 1; i < array.size(); i++) 
		{
			assertFalse("No deber�a existir ese elemento en el heap", array.get(i).equals("H"+ n));
		}
	}

	@Test
	public void sortTest()
	{
		setupEscenario1();
		array.sort(false);

		for (int i = 1; i < array.size(); i++) 
		{
			assertTrue("No deber�a existir ese elemento en el heap", array.get(i).compareTo(array.get(i+1)) >= 0);
		}


	}


}
